﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Poll_Statistic_Android
{

    class Poll
    {
        public class PollOption
        {
            public string text;
            public int rate = 0;

            public PollOption(string text)
            {
                this.text = text;
            }
        }
        public List<PollOption> poll;
        public string pollTitle; 
        public int pollRate = 0;
        public bool isVoted = false;
        public string ID;

        public Poll()
        {
            poll = new List<PollOption>();
        }

        public void setPollTitle(string title)
        {
            pollTitle = title;
        }

        public void AddItem(string text)
        {
            poll.Add(new PollOption(text));
        }

        public void RemoveItem(string text)
        {
            poll.Remove(poll.Find(x => x.text.Equals(text)));
        }

        public void Vote(string text)
        {
            poll.Find(x => x.text.Equals(text)).rate++;
            pollRate++;
            isVoted = true;
        }

        public void CancelVote(string text)
        {
            poll.Find(x => x.text.Equals(text)).rate--;
            pollRate--;
            isVoted = false;
        }

        public int GetRate()
        {
            return pollRate;
        }
    }
}
