﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.Design.Widget;
using Microsoft.WindowsAzure.MobileServices;

namespace Poll_Statistic_Android
{
    /// <summary>
    /// Profile page activity class
    /// </summary>
    [Activity(Label = "Poll_Statistic_Android", MainLauncher = false, Icon = "@drawable/ic_launcher", Theme = "@style/MyTheme")]
    public class ProfileActivity : AppCompatActivity
    {
        //Layouts
        private LinearLayout layout;

        //Vars for work with server part
        private static MobileServiceClient mobileService = CurrentUser.mobileService;
        private List<Polls> serverData;

        private List<Poll> polls;

        /// <summary>
        /// Main function. Runs automatically on app start
        /// </summary>
        /// <param name="bundle"></param>
        protected async override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetTheme(UserSettings.theme);

            SetContentView(Resource.Layout.Profile);

            //Getting access for main layout
            layout = FindViewById<LinearLayout>(Resource.Id.profile_layout);
            layout.SetPadding(50, 50, 50, 50);

            //Log in
            await Authenticate();

            //Getting access for main toolbar
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = GetString(Resource.String.Profile);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            //Creating profile card with photo and user info
            CardView profileCard = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };
            LinearLayout tempLayout = new LinearLayout(this);
            tempLayout.SetPadding(20, 20, 20, 20);
            
            //Filing image view with user avatar
            ImageView profileImage = new ImageView(this);
            profileImage.SetImageResource(Android.Resource.Drawable.SymDefAppIcon);
            tempLayout.AddView(profileImage);

            var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();

            //Output user profile info
            tempLayout.AddView(new TextView(this) { Text = string.Format("Username: {0} {1}", users.Last().firstName, users.Last().secondName), TextSize = 20 });

            //Adding this all to main profile layout
            profileCard.AddView(tempLayout);
            layout.AddView(profileCard);

            //Sync data with server & show feed (async method)
            syncData();
          
        }

        /// <summary>
        /// Filling toolbar with buttons from .xml file
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.feed_top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        /// <summary>
        /// Handling toolbar buttons clicks
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {              
                case Android.Resource.Id.Home:
                    StartActivity(typeof(MainActivity));
                    break;
                case Resource.Id.menu_sync:
                    syncData();
                   
                    return true;
            }
            return base.OnOptionsItemSelected(item);
            
        }

        /// <summary>
        /// Getting access to azure server & getting polls info table (asunc method)
        /// </summary>
        /// <returns></returns>
        private async Task<bool> getServerData()
        {
            MobileServiceInvalidOperationException exeption = (MobileServiceInvalidOperationException)null;
            try
            {
                serverData = await mobileService.GetTable<Polls>().ToListAsync();

                serverData.Reverse();

                serverData.RemoveAll(x => !UserPolls.userPolls.Contains(x.Id));

                var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();

                UserPolls.userPolls = users.Last().userPolls;

                VotedPolls.yourVotedPolls = users.Last().votedPolls;
            }
            catch (MobileServiceInvalidOperationException ex)
            {
                exeption = ex;
            }
            return exeption == null;
        }

        /// <summary>
        /// Showing cards with poll on feed layout
        /// </summary>
        /// <returns></returns>
        private async Task ShowFeed()
        {
            await getServerData();

            foreach(Polls dataCollum in serverData)
            {
                Poll currentPoll = new Poll();

                polls = new List<Poll>();

                currentPoll.pollTitle = dataCollum.pollTitle;
                currentPoll.pollRate = dataCollum.pollRate;
                currentPoll.ID = dataCollum.Id;

                //Convering string that contains poll options titles into list
                foreach (string line in dataCollum.pollOptions.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                    currentPoll.AddItem(line);
                currentPoll.poll.Remove(currentPoll.poll.Last());

                //Convering string that contains poll options rates into list
                string[] strArray = dataCollum.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
                for (int i = 0; i < strArray.Length - 1; i++)
                    currentPoll.poll[i].rate = int.Parse(strArray[i]);

                //Checking if user already vote for this poll
                currentPoll.isVoted = VotedPolls.yourVotedPolls.Contains(dataCollum.Id);
                polls.Add(currentPoll);


                if (!currentPoll.isVoted) // working with unvoted poll
                {
                    //Creating card
                    CardView cardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };

                    LinearLayout tempLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
                    tempLayout.SetPadding(20, 20, 20, 20);

                    //Adding poll title in card
                    var relatLayout = new RelativeLayout(this);

                    relatLayout.AddView(new TextView(this) { Text = currentPoll.pollTitle, TextSize = 20f });

                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
                    p.AddRule(LayoutRules.AlignParentRight);


                    ImageButton popupButton = LayoutInflater.Inflate(Resource.Menu.settingsButton, null) as ImageButton;
                    popupButton.LayoutParameters = p;

                    popupButton.Click += PopupButton_Click1;

                    relatLayout.AddView(popupButton);

                    tempLayout.AddView(relatLayout);

                    //Creatin & adding poll radio buttons to card
                    RadioGroup pollRadioGroup = new RadioGroup(this);

                    for (int j = 0; j < currentPoll.poll.Count; j++)
                    {
                        RadioButton radioButton = new RadioButton(this) { Text = currentPoll.poll[j].text };

                        pollRadioGroup.AddView(radioButton);
                    }
                    pollRadioGroup.CheckedChange += PollRadioGroup_CheckedChange;

                    //Adding this all to main feed layout
                    tempLayout.AddView(pollRadioGroup);
                    cardView.AddView(tempLayout);
                    layout.AddView(cardView);

                }
                else // working with already voted poll
                {
                    //Creating card
                    CardView cardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };

                    LinearLayout linearLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
                    linearLayout.SetPadding(20, 20, 20, 20);

                    //Adding poll title in card
                    var relatLayout = new RelativeLayout(this);

                    relatLayout.AddView(new TextView(this) { Text = currentPoll.pollTitle, TextSize = 20f });

                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
                    p.AddRule(LayoutRules.AlignParentRight);


                    ImageButton popupButton = LayoutInflater.Inflate(Resource.Menu.settingsButton, null) as ImageButton;
                    popupButton.LayoutParameters = p;


                    popupButton.Click += PopupButton_Click1;

                    relatLayout.AddView(popupButton);

                    linearLayout.AddView(relatLayout);

                    //Creatin & adding progress bars to card
                    //Each progress bar shows poll option rating
                    for (int j = 0; j < currentPoll.poll.Count; j++)
                    {
                        //Using relative layout to overlay progress(rating) bar and option title
                        RelativeLayout tempLayout = new RelativeLayout(this);
                        tempLayout.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

                        ProgressBar progressBar = LayoutInflater.Inflate(Resource.Menu.progress_bar, null) as ProgressBar;

                        progressBar.Progress = currentPoll.poll[j].rate;
                        progressBar.Max = currentPoll.GetRate();

                        progressBar.LongClick += ProgressBar_LongClick;

                        //TODO
                        //if (j == getVotedPolls()[serverData[i].Id])
                        //    progressBar.ProgressDrawable.SetColorFilter(Android.Graphics.Color.ParseColor("#00BCD4"), Android.Graphics.PorterDuff.Mode.SrcIn);


                        progressBar.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
                        progressBar.ScaleY = 8f;
                        progressBar.SetPadding(0, reletivePaddingHeight(), 0, reletivePaddingHeight());
                        tempLayout.AddView(progressBar);

                        //Adding otion title on progress bar
                        TextView text = new TextView(this);
                        text.Text = currentPoll.poll[j].text;
                        text.TextSize = 20f;
                        text.SetTextColor(Android.Graphics.Color.Black);
                        text.SetPaddingRelative(10, 10, 0, 0);
                        tempLayout.AddView(text);
                        linearLayout.AddView(tempLayout);

                    }
                    //Adding this all to main feed layout
                    cardView.AddView(linearLayout);
                    layout.AddView(cardView);

                }
                


            }
            
        }

        string currentID;
        int buttonIndex;

        private void PopupButton_Click1(object sender, EventArgs e)
        {
            var button = sender as ImageButton;

            buttonIndex = layout.IndexOfChild(button.Parent.Parent.Parent as View);


            currentID = serverData[buttonIndex - 1].Id;

            var popupMenu = new Android.Widget.PopupMenu(this, sender as ImageButton);

            popupMenu.Inflate(Resource.Menu.popupMenu);

            popupMenu.MenuItemClick += PopupMenu_MenuItemClick;



            popupMenu.Show();
        }

        private async void PopupMenu_MenuItemClick(object sender, Android.Widget.PopupMenu.MenuItemClickEventArgs e)
        {
            switch (e.Item.ItemId)
            {
                case Resource.Id.popup_delete:

                    var list = new List<string>();

                    await getServerData();

                    foreach(Polls poll in serverData)
                        if(poll.Id == currentID)
                        {
                            await mobileService.GetTable<Polls>().DeleteAsync(poll);
                        }

                    foreach (var line in UserPolls.userPolls.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                        list.Add(line);
                    list.Remove(list.Last());

                    list.Remove(currentID);

                    UserPolls.userPolls = "";

                    foreach (var str in list)
                        UserPolls.userPolls += str + "\n";

                    var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();
                    users.Last().userPolls += UserPolls.userPolls;
                    await mobileService.GetTable<Users>().UpdateAsync(users.Last());

                    layout.RemoveViewAt(buttonIndex);
                    break;
                case Resource.Id.popup_edit:
                    foreach (Polls poll in serverData)
                        if (poll.Id == currentID)
                        {
                            TempPoll.tempPoll = poll;
                        }
                    StartActivity(typeof(EditActivity));
                    break;

            }
        }

        /// <summary>
        /// Handling re-vote option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ProgressBar_LongClick(object sender, View.LongClickEventArgs e)
        {

            var currentBar = sender as ProgressBar;


            var currentLayout = currentBar.Parent.Parent as LinearLayout;

            currentLayout.RemoveAllViews();

            //Getting indices of cliked poll option
            int pollIndex = layout.IndexOfChild(currentLayout.Parent as View) - 1;

            var temp = getVotedPolls();

            await getServerData();

            var poll = serverData[pollIndex];

            //Conberting list of rates to string to save it on server &reduce voted option rating by 1

            string[] rates = poll.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            string tmpstr = "";
            for (int i = 0; i < rates.Length - 1; i++)
                tmpstr = i != temp[poll.Id] ? tmpstr + int.Parse(rates[i]) + "\n" : tmpstr + (int.Parse(rates[i]) - 1) + "\n";

            poll.optionsRate = tmpstr;

            poll.pollRate--;

            //Sending info to server
            await mobileService.GetTable<Polls>().UpdateAsync(poll);

            temp.Remove(poll.Id);

            //Updating 
            VotedPolls.yourVotedPolls = "";
            for (int i = 0; i < temp.Count; i++)
            {
                VotedPolls.yourVotedPolls += temp.ElementAt(i).Key + "\n" + temp.ElementAt(i).Value + "\n";
            }

            var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();

            users.Last().votedPolls = VotedPolls.yourVotedPolls;

            await mobileService.GetTable<Users>().UpdateAsync(users.Last());

            Poll currentPoll = new Poll();

            //Parsing data
            currentPoll.pollTitle = poll.pollTitle;
            currentPoll.pollRate = poll.pollRate;
            currentPoll.ID = poll.Id;

            //Convering string that contains poll options titles into list
            foreach (string line in poll.pollOptions.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                currentPoll.AddItem(line);
            currentPoll.poll.Remove(currentPoll.poll.Last());

            //Convering string that contains poll options rates into list
            string[] strArray = poll.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            for (int i = 0; i < strArray.Length - 1; i++)
                currentPoll.poll[i].rate = int.Parse(strArray[i]);




            //Adding poll title in card
            currentLayout.AddView(new TextView(this) { Text = currentPoll.pollTitle, TextSize = 20f });

            //Creatin & adding poll radio buttons to card
            RadioGroup pollRadioGroup = new RadioGroup(this);

            for (int j = 0; j < currentPoll.poll.Count; j++)
            {
                RadioButton radioButton = new RadioButton(this) { Text = currentPoll.poll[j].text };

                pollRadioGroup.AddView(radioButton);
            }

            pollRadioGroup.CheckedChange += PollRadioGroup_CheckedChange;

            //Adding this all to main feed layout
            currentLayout.AddView(pollRadioGroup);

        }


        private int reletivePaddingHeight()
        {
            var dp = (int)((Resources.DisplayMetrics.Ydpi * 25) / 320);
            return dp;
        }

        /// <summary>
        /// Method for handling radio button clic(voting)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void PollRadioGroup_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            //Getting access to clicked radio button (polls option)
            RadioGroup radioGroup = sender as RadioGroup;

            LinearLayout linearLayout = radioGroup.Parent as LinearLayout;

            RadioButton radioButton = FindViewById(e.CheckedId) as RadioButton;

            //Getting indices of cliked poll option
            int pollIndex = layout.IndexOfChild(radioGroup.Parent.Parent as View) - 1;
            int optionIndex = radioGroup.IndexOfChild(radioButton);

            if (!VotedPolls.yourVotedPolls.Contains(serverData[pollIndex].Id))//Fixing bug with several taps 
            {
                //Seving info that user vote this poll
                addVotedPoll(serverData[pollIndex].Id, optionIndex);

                //Sending info to server
                await updateOptionRate(serverData[pollIndex].Id, optionIndex);

                await getServerData();

                var poll = serverData[pollIndex];

                Poll currentPoll = new Poll();

                //Parsing data
                currentPoll.pollTitle = poll.pollTitle;
                currentPoll.pollRate = poll.pollRate;
                currentPoll.ID = poll.Id;

                //Convering string that contains poll options titles into list
                foreach (string line in poll.pollOptions.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                    currentPoll.AddItem(line);
                currentPoll.poll.Remove(currentPoll.poll.Last());

                //Convering string that contains poll options rates into list
                string[] strArray = poll.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
                for (int i = 0; i < strArray.Length - 1; i++)
                    currentPoll.poll[i].rate = int.Parse(strArray[i]);



                //Deleting radio group to replace it with progress(rating) bars
                linearLayout.RemoveView(radioButton.Parent as RadioGroup);

                for (int j = 0; j < currentPoll.poll.Count; ++j)
                {
                    //Using relative layout to overlay progress(rating) bar and option title
                    RelativeLayout tempLayout = new RelativeLayout(this);
                    tempLayout.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

                    ProgressBar progressBar = LayoutInflater.Inflate(Resource.Menu.progress_bar, null) as ProgressBar;

                    progressBar.Progress = currentPoll.poll[j].rate;
                    progressBar.Max = currentPoll.GetRate();

                    //if(j == optionIndex)
                    //    progressBar.ProgressDrawable.SetColorFilter(Android.Graphics.Color.ParseColor("#00BCD4"), Android.Graphics.PorterDuff.Mode.SrcIn);

                    progressBar.LongClick += ProgressBar_LongClick;


                    progressBar.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
                    progressBar.ScaleY = 8f;
                    progressBar.SetPadding(0, reletivePaddingHeight(), 0, reletivePaddingHeight());
                    tempLayout.AddView(progressBar);

                    //Adding otion title on progress bar
                    TextView text = new TextView(this);
                    text.Text = currentPoll.poll[j].text;
                    text.TextSize = 20f;
                    text.SetTextColor(Android.Graphics.Color.Black);
                    text.SetPaddingRelative(10, 0, 0, 0);

                    //Updating card and feed
                    tempLayout.AddView(text);
                    linearLayout.AddView(tempLayout);
                }
            }
        }

        /// <summary>
        /// Update & send info of voted option ro server
        /// </summary>
        /// <param name="pollIndex"></param>
        /// <param name="optionIndex"></param>
        /// <returns></returns>
        private async Task updateOptionRate(string ID, int optionIndex)
        {

            MobileServiceCollection<Polls, Polls> tempColection = await mobileService.GetTable<Polls>().ToCollectionAsync();

            foreach (Polls poll in tempColection)
                if (poll.Id == ID)
                {
                    string[] rates = poll.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
                    string tmpstr = "";
                    for (int i = 0; i < rates.Length - 1; i++)
                        tmpstr = i != optionIndex ? tmpstr + int.Parse(rates[i]) + "\n" : tmpstr + (int.Parse(rates[i]) + 1) + "\n";
                    poll.optionsRate = tmpstr;
                    poll.pollRate++;

                    //Sending info to server
                    await mobileService.GetTable<Polls>().UpdateAsync(poll);
                    break;
                }

            //Conberting list of rates to string to save it on server & Increase voted option rating on 1

        }

        /// <summary>
        /// Method that call outputting feed and show progress dialog on screen. (async method)
        /// </summary>
        private async void syncData()
        {
            if (isOnline())
            {
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.SetTitle(GetString(Resource.String.Sync));
                progressDialog.SetCancelable(false);
                progressDialog.SetCanceledOnTouchOutside(false);
                progressDialog.Show();
                for (int i = 1; i < layout.ChildCount; i++)
                    layout.RemoveViewAt(i);
                           
                await ShowFeed();
                progressDialog.Cancel();
            }
            else
            {
                var builder = new Android.App.AlertDialog.Builder(this);
                builder.SetMessage(GetString(Resource.String.networkError)).SetNeutralButton("OK", (senderAlert, args) => System.Environment.Exit(0));

                var alertDialog = builder.Create();
                alertDialog.Show();
            }
        }

        /// <summary>
        /// Save info of user voted polls
        /// </summary>
        /// <param name="ID"></param>
        private async void addVotedPoll(string ID, int optionIndex)
        {
            var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();

            users.Last().votedPolls += ID + "\n" + optionIndex + "\n";

            await mobileService.GetTable<Users>().UpdateAsync(users.Last());

        }

        /// <summary>
        /// Getting info of user voted polls
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, int> getVotedPolls()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            string[] lines = VotedPolls.yourVotedPolls.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            for (int i = 0; i < lines.Length - 2; i++)
            {
                if (i % 2 == 0)
                    result.Add(lines[i], int.Parse(lines[i + 1]));
            }

            return result;
        }

        private bool isOnline()
        {
            if (((Android.Net.ConnectivityManager)GetSystemService(ConnectivityService)).ActiveNetwork == null)
                return false;
            else return ((Android.Net.ConnectivityManager)GetSystemService(ConnectivityService)).ActiveNetworkInfo.IsConnected;

        }

        //Define a authenticated user.
        private async Task<bool> Authenticate()
        {
            if (mobileService.CurrentUser == null)
            {
                var success = false;
                try
                {
                    // Sign in with Facebook login using a server-managed flow.
                    await mobileService.LoginAsync(this, MobileServiceAuthenticationProvider.MicrosoftAccount);

                    success = true;

                }
                catch (Exception ex)
                {
                    CreateAndShowDialog(ex, GetString(Resource.String.LoginError));
                }
                return success;
            }
            else return true;

        }

        private void CreateAndShowDialog(Exception exception, String title)
        {
            CreateAndShowDialog(exception.Message, title);
        }

        private void CreateAndShowDialog(string message, string title)
        {
            var builder = new Android.App.AlertDialog.Builder(this);

            builder.SetMessage(message);
            builder.SetTitle(title);
            builder.Create().Show();
        }
    }

}
