﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Poll_Statistic_Android
{

    public class Polls
    {
        public string Id { get; set; }
        public string pollTitle { get; set; }
        public int pollRate { get; set; }
        public string pollOptions { get; set; }
        public string optionsRate { get; set; }

    }
}
