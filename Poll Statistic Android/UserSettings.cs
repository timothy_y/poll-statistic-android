using Refractored.Xam.Settings;
using Refractored.Xam.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;

namespace Poll_Statistic_Android
{
    internal static class UserSettings
    {
        public static ISettings AppData
        {
            get
            {
                return CrossSettings.Current;
            }
        }


        private const string key = "userProvider";

        private const string key2 = "firstStart";

        private const string key3 = "theme";




        public static MobileServiceAuthenticationProvider provider
        {
            get
            {
                return AppData.GetValueOrDefault<MobileServiceAuthenticationProvider>(key, MobileServiceAuthenticationProvider.MicrosoftAccount);
            }
            set
            {
                AppData.AddOrUpdateValue(key, value);
            }
        }

        public static bool isFirstStart
        {
            get
            {
                return AppData.GetValueOrDefault<bool>(key2, true);
            }
            set
            {
                AppData.AddOrUpdateValue(key2, value);
            }
        }

        public static int theme
        {
            get
            {
                return AppData.GetValueOrDefault<int>(key3, Resource.Style.MyTheme);
            }
            set
            {
                AppData.AddOrUpdateValue(key3, value);
            }
        }

    }
}