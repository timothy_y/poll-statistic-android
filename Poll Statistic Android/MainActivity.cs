﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.Design.Widget;
using Microsoft.WindowsAzure.MobileServices;


namespace Poll_Statistic_Android
{

    /// <summary>
    /// Feed page activity class. Runs automatically on app start
    /// </summary>
    [Activity(Icon = "@drawable/ic_launcher", Label = "Poll Statistic", MainLauncher = true, Theme = "@style/MyTheme")]
    public class MainActivity : AppCompatActivity
    {       
        //Layouts
        private DrawerLayout drawerLayout;
        private NavigationView navigationView;
        private LinearLayout layout;

        //Vars for work with server part
        private static MobileServiceClient mobileService = CurrentUser.mobileService;
        private List<Polls> serverData;


        /// <summary>
        /// Main function. Runs automatically on app start
        /// </summary>
        /// <param name="bundle"></param>
        protected override void OnCreate(Bundle bundle)
        {
            SetTheme(UserSettings.theme);

            base.OnCreate(bundle);
            
            SetContentView(Resource.Layout.Main);

            //Getting access for main layout
            layout = FindViewById<LinearLayout>(Resource.Id.feed_layout);
            layout.SetPadding(50, 50, 50, 50);

            //Getting access for main toolbar
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu_white_24dp);
            SupportActionBar.Title = GetString(Resource.String.Feed);

            //Getting access for left nav bar
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.NavigationItemSelected += OnNavigationItemSelected;

            //Getting access for floatin "+" button
            FloatingActionButton fabButton = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fabButton.Click += FabButton_Click;

            //Sync data with server & show feed (async method)
            if (UserSettings.isFirstStart)
                onLoginDialogCreate();
            else
                syncData();       

        }

        /// <summary>
        /// Floating button method for handling clicks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FabButton_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(AddPageActivity));
        }

        /// <summary>
        /// Filling toolbar with buttons from .xml file
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.feed_top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        /// <summary>
        /// Handling toolbar buttons clicks
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    if (drawerLayout.IsDrawerOpen(Android.Support.V4.View.GravityCompat.Start))
                    {
                        drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                        break;
                    }
                    this.drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    break;
                case Resource.Id.menu_sync:
                    syncData();
                    break;
                case Resource.Id.menu_search:

                    var searchEdit = new EditText(this);
                    Android.App.AlertDialog alertDialog;
                    var builder = new Android.App.AlertDialog.Builder(this);
                    builder.SetTitle(GetString(Resource.String.Search)).SetPositiveButton("OK", (senderAlert, args) => Search(searchEdit.Text)).SetNegativeButton(GetString(Resource.String.cancel), (senderAlert, args) => alertDialog = null); ;

                    alertDialog = builder.Create();
                    alertDialog.SetView(searchEdit);                    
                    alertDialog.Show();

                    break;

            }
            return base.OnOptionsItemSelected(item);
        }

        /// <summary>
        /// Handling left nav bar buttons clicks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            IMenuItem menuItem = e.MenuItem;
            menuItem.SetChecked(!menuItem.IsChecked);
            switch (menuItem.ItemId)
            {
                case Resource.Id.nav_profile:
                    StartActivity(typeof(ProfileActivity));
                    break;
                case Resource.Id.nav_add:
                    StartActivity(typeof(AddPageActivity));
                    break;
                case Resource.Id.nav_settings:
                    StartActivity(typeof(SettingActivity));
                    break;
                case Resource.Id.nav_About:
                    StartActivity(typeof(AboutActivity));
                    break;

            }
        }

        /// <summary>
        /// Getting access to azure server & getting polls info table (asunc method)
        /// </summary>
        /// <returns></returns>
        private async Task<bool> getServerData()
        {
            MobileServiceInvalidOperationException exeption = (MobileServiceInvalidOperationException)null;
            try
            {
                serverData = await mobileService.GetTable<Polls>().ToListAsync();

                serverData.Reverse();

                var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();

                VotedPolls.yourVotedPolls = users.Last().votedPolls; 
            }
            catch (MobileServiceInvalidOperationException ex)
            {
                exeption = ex;
            }
            return exeption == null;
        }

        /// <summary>
        /// Showing cards with poll on feed layout
        /// </summary>
        /// <returns></returns>
        private void ShowFeed()
        {
            foreach (Polls dataCollum in serverData)
            {
                Poll currentPoll = new Poll();          

                //Parsing data
                currentPoll.pollTitle = dataCollum.pollTitle;
                currentPoll.pollRate = dataCollum.pollRate;
                currentPoll.ID = dataCollum.Id;

                //Convering string that contains poll options titles into list
                foreach (string line in dataCollum.pollOptions.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                    currentPoll.AddItem(line);
                currentPoll.poll.Remove(currentPoll.poll.Last());

                //Convering string that contains poll options rates into list
                string[] strArray = dataCollum.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
                for (int i = 0; i < strArray.Length - 1; i++)
                    currentPoll.poll[i].rate = int.Parse(strArray[i]);

                //Checking if user already vote for this poll
                currentPoll.isVoted = VotedPolls.yourVotedPolls.Contains(dataCollum.Id);

                if (!currentPoll.isVoted) // working with unvoted poll
                {
                    //Creating card
                    CardView cardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };

                    LinearLayout tempLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
                    tempLayout.SetPadding(20, 20, 20, 20);

                    //Adding poll title in card                 
                    tempLayout.AddView(new TextView(this) { Text = currentPoll.pollTitle, TextSize = 20f });

                    //Creatin & adding poll radio buttons to card
                    RadioGroup pollRadioGroup = new RadioGroup(this);

                    for (int j = 0; j < currentPoll.poll.Count; j++)
                    {
                        RadioButton radioButton = new RadioButton(this) { Text = currentPoll.poll[j].text };

                        pollRadioGroup.AddView(radioButton);
                    }
                    pollRadioGroup.CheckedChange += PollRadioGroup_CheckedChange;

                    //Adding this all to main feed layout
                    tempLayout.AddView(pollRadioGroup);
                    cardView.AddView(tempLayout);
                    layout.AddView(cardView);

                }
                else // working with already voted poll
                {
                    //Creating card
                    CardView cardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };

                    LinearLayout linearLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
                    linearLayout.SetPadding(20, 20, 20, 20);

                    //Adding poll title in card
                    linearLayout.AddView(new TextView(this) { Text = currentPoll.pollTitle, TextSize = 20f });


                    //Creatin & adding progress bars to card
                    //Each progress bar shows poll option rating
                    for (int j = 0; j < currentPoll.poll.Count; j++)
                    {
                        //Using relative layout to overlay progress(rating) bar and option title
                        RelativeLayout tempLayout = new RelativeLayout(this);
                        tempLayout.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

                        ProgressBar progressBar = LayoutInflater.Inflate(Resource.Menu.progress_bar, null) as ProgressBar;

                        progressBar.Progress = currentPoll.poll[j].rate;
                        progressBar.Max = currentPoll.GetRate();

                        progressBar.LongClick += ProgressBar_LongClick;

                        //TODO
                        //if (j == getVotedPolls()[serverData[i].Id])
                        //    progressBar.ProgressDrawable.SetColorFilter(Android.Graphics.Color.ParseColor("#00BCD4"), Android.Graphics.PorterDuff.Mode.SrcIn);


                        progressBar.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
                        progressBar.ScaleY = 8f;
                        progressBar.SetPadding(0, reletivePaddingHeight(), 0, reletivePaddingHeight());
                        tempLayout.AddView(progressBar);

                        //Adding otion title on progress bar
                        TextView text = new TextView(this);
                        text.Text = currentPoll.poll[j].text;
                        text.SetTextColor(Android.Graphics.Color.Black);
                        text.SetPaddingRelative(10, 10, 0, 0);
                        text.TextSize = reletiveTextSize();
                        text.TextAlignment = TextAlignment.Center;
                        

                        

                        tempLayout.AddView(text);
                        linearLayout.AddView(tempLayout);

                    }
                    //Adding this all to main feed layout
                    cardView.AddView(linearLayout);
                    layout.AddView(cardView);

                }


            }
        }

        private int reletivePaddingHeight()
        {
            var dp = (int)((Resources.DisplayMetrics.Ydpi * 25) / 320);
            return dp;
        }
        private int reletiveTextSize()
        {
            var dp = (int)((Resources.DisplayMetrics.WidthPixels * 25) / 2560);
            return dp;
        }

        /// <summary>
        /// Method for handling radio button clic(voting)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void PollRadioGroup_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            
            //Getting access to clicked radio button (polls option)
            RadioGroup radioGroup = sender as RadioGroup;

            LinearLayout linearLayout = radioGroup.Parent as LinearLayout;

            RadioButton radioButton = FindViewById(e.CheckedId) as RadioButton;

            //Getting indices of cliked poll option
            int pollIndex = layout.IndexOfChild(radioGroup.Parent.Parent as View);
            int optionIndex = radioGroup.IndexOfChild(radioButton);

            if (!VotedPolls.yourVotedPolls.Contains(serverData[pollIndex].Id))//Fixing bug with several taps 
            {
                //Seving info that user vote this poll
                addVotedPoll(serverData[pollIndex].Id, optionIndex);

                //Sending info to server
                await updateOptionRate(serverData[pollIndex].Id, optionIndex);

                await getServerData();

                var poll = serverData[pollIndex];

                Poll currentPoll = new Poll();

                //Parsing data
                currentPoll.pollTitle = poll.pollTitle;
                currentPoll.pollRate = poll.pollRate;
                currentPoll.ID = poll.Id;

                //Convering string that contains poll options titles into list
                foreach (string line in poll.pollOptions.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                    currentPoll.AddItem(line);
                currentPoll.poll.Remove(currentPoll.poll.Last());

                //Convering string that contains poll options rates into list
                string[] strArray = poll.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
                for (int i = 0; i < strArray.Length - 1; i++)
                    currentPoll.poll[i].rate = int.Parse(strArray[i]);



                //Deleting radio group to replace it with progress(rating) bars
                linearLayout.RemoveView(radioButton.Parent as RadioGroup);

                for (int j = 0; j < currentPoll.poll.Count; ++j)
                {
                    //Using relative layout to overlay progress(rating) bar and option title
                    RelativeLayout tempLayout = new RelativeLayout(this);
                    tempLayout.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

                    ProgressBar progressBar = LayoutInflater.Inflate(Resource.Menu.progress_bar, null) as ProgressBar;

                    progressBar.Progress = currentPoll.poll[j].rate;
                    progressBar.Max = currentPoll.GetRate();

                    //if(j == optionIndex)
                    //    progressBar.ProgressDrawable.SetColorFilter(Android.Graphics.Color.ParseColor("#00BCD4"), Android.Graphics.PorterDuff.Mode.SrcIn);

                    progressBar.LongClick += ProgressBar_LongClick;


                    progressBar.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
                    progressBar.ScaleY = 8f;
                    progressBar.SetPadding(0, reletivePaddingHeight(), 0, reletivePaddingHeight());
                    tempLayout.AddView(progressBar);

                    //Adding otion title on progress bar
                    TextView text = new TextView(this);
                    text.Text = currentPoll.poll[j].text;
                    text.SetTextColor(Android.Graphics.Color.Black);
                    text.Gravity = GravityFlags.Fill;
                    text.SetPaddingRelative(10, 10, 0, 0);
                   

                    //Updating card and feed
                    tempLayout.AddView(text);
                    linearLayout.AddView(tempLayout);
                }
            }
        }

        /// <summary>
        /// Handling re-vote option 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ProgressBar_LongClick(object sender, View.LongClickEventArgs e)
        {

            var currentBar = sender as ProgressBar;


            var currentLayout = currentBar.Parent.Parent as LinearLayout;

            currentLayout.RemoveAllViews();

            //Getting indices of cliked poll option
            int pollIndex = layout.IndexOfChild(currentLayout.Parent as View);

            var temp = getVotedPolls();

            await getServerData();     

            var poll = serverData[pollIndex];

            //Conberting list of rates to string to save it on server &reduce voted option rating by 1

            string[] rates = poll.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            string tmpstr = "";
            for (int i = 0; i < rates.Length - 1; i++)
                tmpstr = i != temp[poll.Id] ? tmpstr + int.Parse(rates[i]) + "\n" : tmpstr + (int.Parse(rates[i]) - 1) + "\n";

            poll.optionsRate = tmpstr;

            poll.pollRate--;

            //Sending info to server
            await mobileService.GetTable<Polls>().UpdateAsync(poll);

            temp.Remove(poll.Id);

            //Updating 
            VotedPolls.yourVotedPolls = "";
            for (int i = 0; i < temp.Count; i++)
            {
                VotedPolls.yourVotedPolls += temp.ElementAt(i).Key + "\n" + temp.ElementAt(i).Value + "\n";
            }

            var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();

            users.Last().votedPolls = VotedPolls.yourVotedPolls;

            await mobileService.GetTable<Users>().UpdateAsync(users.Last());


            Poll currentPoll = new Poll();

            //Parsing data
            currentPoll.pollTitle = poll.pollTitle;
            currentPoll.pollRate = poll.pollRate;
            currentPoll.ID = poll.Id;

            //Convering string that contains poll options titles into list
            foreach (string line in poll.pollOptions.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                currentPoll.AddItem(line);
            currentPoll.poll.Remove(currentPoll.poll.Last());

            //Convering string that contains poll options rates into list
            string[] strArray = poll.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            for (int i = 0; i < strArray.Length - 1; i++)
                currentPoll.poll[i].rate = int.Parse(strArray[i]);




            //Adding poll title in card
            currentLayout.AddView(new TextView(this) { Text = currentPoll.pollTitle, TextSize = 20f });

            //Creatin & adding poll radio buttons to card
            RadioGroup pollRadioGroup = new RadioGroup(this);

            for (int j = 0; j < currentPoll.poll.Count; j++)
            {
                RadioButton radioButton = new RadioButton(this) { Text = currentPoll.poll[j].text };

                pollRadioGroup.AddView(radioButton);
            }

            pollRadioGroup.CheckedChange += PollRadioGroup_CheckedChange;

            //Adding this all to main feed layout
            currentLayout.AddView(pollRadioGroup);

        }

        /// <summary>
        /// Converting server info to comfortable list of polls to easily output them
        /// </summary>

        /// <summary>
        /// Update & send info of voted option ro server
        /// </summary>
        /// <param name="pollIndex"></param>
        /// <param name="optionIndex"></param>
        /// <returns></returns>
        private async Task updateOptionRate(string ID, int optionIndex)
        {
            
            foreach(Polls poll in serverData)
                if(poll.Id == ID)
                {
                    //Conberting list of rates to string to save it on server & Increase voted option rating on 1
                    string[] rates = poll.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
                    string tmpstr = "";
                    for (int i = 0; i < rates.Length - 1; i++)
                        tmpstr = i != optionIndex ? tmpstr + int.Parse(rates[i]) + "\n" : tmpstr + (int.Parse(rates[i]) + 1) + "\n";
                    poll.optionsRate = tmpstr;

                    poll.pollRate++;

                    //Sending info to server
                    await mobileService.GetTable<Polls>().UpdateAsync(poll);
                    break;
                }          
           
        }

        /// <summary>
        /// Method that call outputting feed and show progress dialog on screen. (async method)
        /// </summary>
        private async void syncData()
        {
            if (isOnline())
            {
                layout.RemoveAllViews();
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.SetTitle(GetString(Resource.String.Sync));
                progressDialog.SetCancelable(false);
                progressDialog.SetCanceledOnTouchOutside(false);
                progressDialog.Show();
                if (mobileService.CurrentUser == null)
                {
                    await Authenticate(UserSettings.provider);
                }
                var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();
                if (users.Count != 0)
                {
                    

                    await getServerData();

                    ShowFeed();

                    progressDialog.Cancel();
                }                    
            }
            else
            {
                var builder = new Android.App.AlertDialog.Builder(this);
                builder.SetMessage(GetString(Resource.String.networkError)).SetNeutralButton("OK", (senderAlert, args) => System.Environment.Exit(0));

                var alertDialog = builder.Create();
                alertDialog.Show();
            }
        }

        /// <summary>
        /// Save info of user voted polls
        /// </summary>
        /// <param name="ID"></param>
        private async void addVotedPoll(string ID, int optionIndex)
        {
            var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();

            users.Last().votedPolls += ID + "\n" + optionIndex + "\n";

            await mobileService.GetTable<Users>().UpdateAsync(users.Last());

        }

        /// <summary>
        /// Getting info of user voted polls
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, int> getVotedPolls()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            string[] lines = VotedPolls.yourVotedPolls.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            for(int i = 0; i < lines.Length - 1; i++)
            {
                if (i % 2 == 0)
                    result.Add(lines[i], int.Parse(lines[i + 1]));
            }
        
            return result;
        }



        private bool isOnline()
        {
            if (((Android.Net.ConnectivityManager)GetSystemService(ConnectivityService)).ActiveNetwork == null)
                return false;
            else return ((Android.Net.ConnectivityManager)GetSystemService(ConnectivityService)).ActiveNetworkInfo.IsConnectedOrConnecting;

        }

        private async void Search(string param)
        {
            layout.RemoveAllViews();
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.SetTitle(Resource.String.Search);
            progressDialog.Show();

            await getServerData();

            foreach (Polls dataCollum in serverData)
            {
                if (dataCollum.pollTitle.Contains(param) || dataCollum.pollOptions.Contains(param))
                {
                    Poll currentPoll = new Poll();

                    //Parsing data
                    currentPoll.pollTitle = dataCollum.pollTitle;
                    currentPoll.pollRate = dataCollum.pollRate;
                    currentPoll.ID = dataCollum.Id;

                    //Convering string that contains poll options titles into list
                    foreach (string line in dataCollum.pollOptions.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                        currentPoll.AddItem(line);
                    currentPoll.poll.Remove(currentPoll.poll.Last());

                    //Convering string that contains poll options rates into list
                    string[] strArray = dataCollum.optionsRate.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
                    for (int i = 0; i < strArray.Length - 1; i++)
                        currentPoll.poll[i].rate = int.Parse(strArray[i]);

                    //Checking if user already vote for this poll
                    currentPoll.isVoted = VotedPolls.yourVotedPolls.Contains(dataCollum.Id);

                    if (!currentPoll.isVoted) // working with unvoted poll
                    {
                        //Creating card
                        CardView cardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };

                        LinearLayout tempLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
                        tempLayout.SetPadding(20, 20, 20, 20);

                        //Adding poll title in card                 
                        tempLayout.AddView(new TextView(this) { Text = currentPoll.pollTitle, TextSize = 20f });

                        //Creatin & adding poll radio buttons to card
                        RadioGroup pollRadioGroup = new RadioGroup(this);

                        for (int j = 0; j < currentPoll.poll.Count; j++)
                        {
                            RadioButton radioButton = new RadioButton(this) { Text = currentPoll.poll[j].text };

                            pollRadioGroup.AddView(radioButton);
                        }
                        pollRadioGroup.CheckedChange += PollRadioGroup_CheckedChange;

                        //Adding this all to main feed layout
                        tempLayout.AddView(pollRadioGroup);
                        cardView.AddView(tempLayout);
                        layout.AddView(cardView);

                    }
                    else // working with already voted poll
                    {
                        //Creating card
                        CardView cardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };

                        LinearLayout linearLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
                        linearLayout.SetPadding(20, 20, 20, 20);

                        //Adding poll title in card
                        linearLayout.AddView(new TextView(this) { Text = currentPoll.pollTitle, TextSize = 20f });


                        //Creatin & adding progress bars to card
                        //Each progress bar shows poll option rating
                        for (int j = 0; j < currentPoll.poll.Count; j++)
                        {
                            //Using relative layout to overlay progress(rating) bar and option title
                            RelativeLayout tempLayout = new RelativeLayout(this);
                            tempLayout.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

                            ProgressBar progressBar = LayoutInflater.Inflate(Resource.Menu.progress_bar, null) as ProgressBar;

                            progressBar.Progress = currentPoll.poll[j].rate;
                            progressBar.Max = currentPoll.GetRate();

                            progressBar.LongClick += ProgressBar_LongClick;

                            //TODO
                            //if (j == getVotedPolls()[serverData[i].Id])
                            //    progressBar.ProgressDrawable.SetColorFilter(Android.Graphics.Color.ParseColor("#00BCD4"), Android.Graphics.PorterDuff.Mode.SrcIn);


                            progressBar.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
                            progressBar.ScaleY = 8f;
                            progressBar.SetPadding(0, reletivePaddingHeight(), 0, reletivePaddingHeight());
                            tempLayout.AddView(progressBar);

                            //Adding otion title on progress bar
                            TextView text = new TextView(this);
                            text.Text = currentPoll.poll[j].text;
                            text.TextSize = 20f;
                            text.SetTextColor(Android.Graphics.Color.Black);
                            text.SetPaddingRelative(10, 10, 0, 0);
                            tempLayout.AddView(text);
                            linearLayout.AddView(tempLayout);

                        }
                        //Adding this all to main feed layout
                        cardView.AddView(linearLayout);
                        layout.AddView(cardView);
                    }

                }
                

            }

            progressDialog.Cancel();

        }

        private async Task<bool> Authenticate(MobileServiceAuthenticationProvider provider)
        {
            if (mobileService.CurrentUser == null)
            {
                var success = false;
                try
                {

                    // Sign in with Facebook login using a server-managed flow.
                    await mobileService.LoginAsync(this, provider);

                    var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();

                    if (users.Count == 0)
                    {
                        var tempLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
                        tempLayout.SetPadding(10, 0, 10, 0);

                        tempLayout.AddView(new TextView(this) { Text = GetString(Resource.String.firstName), TextSize = 20f });

                        var firstNameEdit = new EditText(this) { Hint = GetString(Resource.String.firstNameHint) };
                        tempLayout.AddView(firstNameEdit);

                        tempLayout.AddView(new TextView(this) { Text = GetString(Resource.String.secondName), TextSize = 20f });

                        var secondNameEdit = new EditText(this) { Hint = GetString(Resource.String.secondNameHint) };
                        tempLayout.AddView(secondNameEdit);

                        Android.App.AlertDialog alertDialog;
                        var builder = new Android.App.AlertDialog.Builder(this);

                        builder.SetTitle(GetString(Resource.String.Regist)).SetPositiveButton("OK", async (senderAlert, args) =>
                        {
                            await mobileService.GetTable<Users>().InsertAsync(new Users()
                            {
                                firstName = firstNameEdit.Text,
                                secondName = secondNameEdit.Text,
                                userId = mobileService.CurrentUser.UserId
                            });

                        }).SetNegativeButton(GetString(Resource.String.cancel), (senderAlert, args) => alertDialog = null); ;

                        alertDialog = builder.Create();
                        alertDialog.SetView(tempLayout);
                        alertDialog.SetCanceledOnTouchOutside(false);
                        alertDialog.SetCancelable(false);
                        alertDialog.Show();
                    }

                    success = true;

                }
                catch (Exception ex)
                {
                    CreateAndShowDialog(ex, GetString(Resource.String.LoginError));
                }
                return success;
            }
            else return true;

        }

        private void CreateAndShowDialog(Exception exception, String title)
        {
            CreateAndShowDialog(exception.Message, title);
        }

        private void CreateAndShowDialog(string message, string title)
        {
            var builder = new Android.App.AlertDialog.Builder(this);

            builder.SetMessage(message);
            builder.SetTitle(title);
            builder.Create().Show();
        }

        private void onLoginDialogCreate()
        {
            var builder = new Android.App.AlertDialog.Builder(this);

            builder.SetTitle(Resource.String.Login);
            Android.App.AlertDialog alertDialog = null;
            var tempLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
            tempLayout.SetPadding(10, 0, 10, 0);

            var facebookButton = new Button(this) { Text = "Facebook" };
            facebookButton.Click += async (senderAlert, args) =>
            {
                await Authenticate(MobileServiceAuthenticationProvider.Facebook);
                UserSettings.provider = MobileServiceAuthenticationProvider.Facebook;
                dialogClose(alertDialog);
            };
            tempLayout.AddView(facebookButton);

            var googleButton = new Button(this) { Text = "Google" };
            googleButton.Click += async (senderAlert, args) =>
            {
                await Authenticate(MobileServiceAuthenticationProvider.Google);
                UserSettings.provider = MobileServiceAuthenticationProvider.Google;
                dialogClose(alertDialog);
            };
            tempLayout.AddView(googleButton);

            var microsoftButton = new Button(this) { Text = "Microsoft" };
            microsoftButton.Click += async (senderAlert, args) =>
            {
                await Authenticate(MobileServiceAuthenticationProvider.MicrosoftAccount);
                UserSettings.provider = MobileServiceAuthenticationProvider.MicrosoftAccount;
                dialogClose(alertDialog);

            };
            tempLayout.AddView(microsoftButton);

           

            alertDialog = builder.Create();
            alertDialog.SetView(tempLayout);
            alertDialog.SetCanceledOnTouchOutside(false);
            alertDialog.SetCancelable(false);
            alertDialog.Show();
            UserSettings.isFirstStart = false;

        }
        private void dialogClose(Android.App.AlertDialog dialog)
        {
            dialog.Cancel();
        }

    }
}
