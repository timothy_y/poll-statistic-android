﻿using Refractored.Xam.Settings;
using Refractored.Xam.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poll_Statistic_Android
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    internal static class VotedPolls
    {

        public static ISettings AppData
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        
        private const string key = "votedPolls";
        private const string key2 = "votedOptions";
        
        private static readonly string SettingsDefault = "";


        public static string yourVotedPolls 
        {
            get
            {
                return AppData.GetValueOrDefault<string>(key, SettingsDefault);
            }
            set
            {
                AppData.AddOrUpdateValue(key, value);
            }
        }

    }

}
