using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Poll_Statistic_Android
{
    class Users
    {
        public string Id { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string userPolls { get; set; }
        public string votedPolls { get; set; }
        public string userId { get; set; }
    }
}